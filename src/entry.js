import './styles/styles.scss'
import $ from 'jquery'

const $app = $('#app')
const $msg = $app.find('.message')

$app.on('click', 'button', function() {
    let msgText = 'Meh...';
    if (this.hasAttribute('primary')) {
        msgText = 'Primary'
    } else if (this.hasAttribute('alert')) {
        msgText = 'ALERT!'
    }

    $msg.html(msgText);
});
