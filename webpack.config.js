var path = require('path');
var WriteFilePlugin = require('write-file-webpack-plugin');  // force webpack-dev-server to write to disk
var DashboardPlugin = require('webpack-dashboard/plugin');

module.exports = {
    entry: path.resolve(__dirname, 'src', 'entry.js'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "bundle.js",
    },
    devServer: {
        outputPath: path.resolve(__dirname, 'dist')
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            {
              test: /\.js$/,
              exclude: /(node_modules|bower_components)/,
              loader: 'babel-loader',
              query: {
                presets: ['es2015'],
              },
            },
            {
              test: /\.scss$/,
              loaders: ["style-loader", "css-loader", "sass-loader"],
            },
        ],
    },
    plugins: [
        new WriteFilePlugin(),
        new DashboardPlugin(),
    ],
};
